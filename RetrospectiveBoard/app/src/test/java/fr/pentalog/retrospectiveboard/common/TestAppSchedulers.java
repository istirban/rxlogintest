package fr.pentalog.retrospectiveboard.common;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Ionut Stirban on 03/02/2017.
 */

public class TestAppSchedulers implements AppSchedulers {

    @Override
    public Scheduler io() {
        return Schedulers.trampoline();
    }

    @Override
    public Scheduler mainThread() {
        return Schedulers.trampoline();
    }
}
