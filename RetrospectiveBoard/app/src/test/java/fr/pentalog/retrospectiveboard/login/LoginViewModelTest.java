package fr.pentalog.retrospectiveboard.login;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.pentalog.retrospectiveboard.common.TestAppSchedulers;
import fr.pentalog.retrospectiveboard.common.ViewLifecycle;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Created by Ionut Stirban on 03/02/2017.
 */
public class LoginViewModelTest {
    @Mock
    LoginModel model;

    private LoginViewModel viewModel;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        viewModel = new LoginViewModel(model, new ViewLifecycle());
        viewModel.setAppSchedulers(new TestAppSchedulers());
    }

    @Test
    public void should_show_error_When_account_is_invalid() throws Exception {

        TestObserver<Integer> loginErrorObserver = new TestObserver<>();

        viewModel.errors.loginFailed
                .subscribe(loginErrorObserver);

        when(model.login(anyString(), anyString())).thenReturn(Observable.just(false));

        viewModel.onLoginClicked();

        loginErrorObserver.assertValueCount(1);



    }


    @Test
    public void should_show_success_When_account_is_valid() throws Exception {

        final String user = "ionut";
        final String password = "123";
        viewModel.inputs.name.set(user);
        viewModel.inputs.password.set(password);

        TestObserver<Integer> loginSuccessObserver = new TestObserver<>();

        viewModel.outputs.loginSuccess
                .subscribe(loginSuccessObserver);

        when(model.login(user,password)).thenReturn(Observable.just(true));

        viewModel.onLoginClicked();

        loginSuccessObserver.assertValueCount(1);


    }
}