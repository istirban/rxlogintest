package fr.pentalog.retrospectiveboard.login;


import android.os.Looper;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

/**
 * Created by Ionut Stirban on 02/02/2017.
 */

public class LoginModel {
    public Observable<Boolean> login(final String username, final String password) {
        Log.d("test", "user: " + username + " password: " + password);
        return Observable.create(new ObservableOnSubscribe<Boolean>() {
            @Override
            public void subscribe(ObservableEmitter<Boolean> e) throws Exception {
                Log.d("test", "thread: " + (Looper.myLooper() == Looper.getMainLooper()));
                if (username.equals("ionut") && password.equals("123")) {
                    e.onNext(true);
                } else {
                    e.onNext(false);
                }
            }
        }).delay(2, TimeUnit.SECONDS);
    }
}
