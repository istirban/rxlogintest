package fr.pentalog.retrospectiveboard.common.http;

public class ApisManager {
    private ApisManager() {
        throw new AssertionError("No instance allowed");
    }

    public static <T> T getApi(Class<T> clazz) {
        return RetrofitBuilder.getRetrofitInstance().create(clazz);
    }

}