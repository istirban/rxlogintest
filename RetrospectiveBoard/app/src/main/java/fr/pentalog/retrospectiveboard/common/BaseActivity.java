package fr.pentalog.retrospectiveboard.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by Ionut Stirban on 02/02/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {
    protected ViewLifecycle viewLifecycle = new ViewLifecycle();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewLifecycle.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        viewLifecycle.onPause();
    }

    @Override
    protected void onDestroy() {
        viewLifecycle.onDestroy();
        super.onDestroy();

    }

    private <T> ObservableTransformer<T, T> bindUntilDestroy() {
        return viewLifecycle.bindUntilDestroy();
    }

    protected <T> ObservableTransformer<T, T> bindToUiUntilDestroy() {
        return upstream -> upstream.compose(bindUntilDestroy())
                .observeOn(AndroidSchedulers.mainThread());
    }

//    protected void setContentView() {
//        setContentView(getLayoutResId());
//    }
//
//    @LayoutRes
//    protected abstract int getLayoutResId();
}
