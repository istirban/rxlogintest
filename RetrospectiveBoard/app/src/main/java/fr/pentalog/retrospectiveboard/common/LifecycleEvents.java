package fr.pentalog.retrospectiveboard.common;

/**
 * Created by Ionut Stirban on 02/02/2017.
 */

public class LifecycleEvents {
    public static final int RESUMED = 1;
    public static final int PAUSED = 2;
    public static final int DESTROYED = 4;
}
