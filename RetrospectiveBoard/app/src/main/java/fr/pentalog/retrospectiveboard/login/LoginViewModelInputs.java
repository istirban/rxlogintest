package fr.pentalog.retrospectiveboard.login;

import android.databinding.ObservableField;

/**
 * Created by Ionut Stirban on 03/02/2017.
 */

public class LoginViewModelInputs {
    public final ObservableField<String> name = new ObservableField<>("");
    public final ObservableField<String> password = new ObservableField<>("");
}
