package fr.pentalog.retrospectiveboard.login.data;

import fr.pentalog.retrospectiveboard.common.http.ApisManager;
import io.reactivex.Completable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Ionut Stirban on 03/02/2017.
 */

public class LoginRepository {
    private LoginApi loginApi;

    public LoginRepository() {
        loginApi = ApisManager.getApi(LoginApi.class);
    }

    public Completable login(String username, String password) {
        final LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername(username);
        loginRequest.setPassword(password);

        return loginApi.login(loginRequest)
                .subscribeOn(Schedulers.io());
    }
}
