package fr.pentalog.retrospectiveboard.common;


import io.reactivex.ObservableOperator;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by Ionut Stirban on 02/02/2017.
 */

public class LifecycleOperator<T> implements ObservableOperator<T, T> {
    private BehaviorSubject<Integer> viewLifecycle;
    private T lastEvent = null;
    private Observer<? super T> decoratedObserver;
    private Observer<T> observer;

    public LifecycleOperator(BehaviorSubject<Integer> viewLifecycle) {
        this.viewLifecycle = viewLifecycle;
        this.viewLifecycle.subscribe(lifecycleEvent -> {
            if (lifecycleEvent == LifecycleEvents.RESUMED && lastEvent != null && decoratedObserver != null) {
                decoratedObserver.onNext(lastEvent);
            } else if (lifecycleEvent == LifecycleEvents.DESTROYED && decoratedObserver != null) {

            }
        });
    }

    @Override
    public Observer<? super T> apply(Observer<? super T> decoratedObserver) throws Exception {
        this.decoratedObserver = decoratedObserver;

        observer = new Observer<T>() {
            @Override
            public void onSubscribe(Disposable d) {
                decoratedObserver.onSubscribe(d);
            }

            @Override
            public void onNext(T t) {
                if (viewLifecycle.hasValue() && LifecycleEvents.RESUMED == viewLifecycle.getValue()) {
                    decoratedObserver.onNext(t);
                    lastEvent = null;
                } else {
                    lastEvent = t;
                }

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        observer.onSubscribe(new Disposable() {
            @Override
            public void dispose() {

            }

            @Override
            public boolean isDisposed() {
                return false;
            }
        });
        return observer;
    }
}
