package fr.pentalog.retrospectiveboard.login.data;


import io.reactivex.Completable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LoginApi {

    @POST("provider/login")
    Completable login(@Body LoginRequest request);
}
