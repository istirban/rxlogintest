package fr.pentalog.retrospectiveboard.common.http;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class RetrofitBuilder {
    private static final String URL = "";//todo
    private static final String USER_AGENT = "Android;Mobile";

    private static Retrofit retrofit;
    private static Retrofit localRetrofit;

    private RetrofitBuilder() {
        throw new AssertionError("No instance allowed");
    }

    static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = newRetrofit(URL);
        }

        return retrofit;
    }


    static void refreshRetrofitInstance() {
        retrofit = newRetrofit(URL);
    }


    private static Retrofit newRetrofit(String url) {
        final Interceptor interceptor = RetrofitBuilder.newInterceptor();
        final OkHttpClient httpClient = RetrofitBuilder.newOkHttpClient(interceptor);

        return new Retrofit.Builder()
                .baseUrl(url)
                .client(httpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonFactory.createGson()))
                .build();
    }

    @NonNull
    private static Interceptor newInterceptor() {
        return chain -> {
            Request original = chain.request();

            final String token = InMemoryTokenManager.getInstance().getToken();
            // Request customization: add request headers
            Request.Builder requestBuilder = newRequestBuilder(original, token);

            Request request = requestBuilder.build();
            return chain.proceed(request);

        };
    }

    @NonNull
    private static OkHttpClient newOkHttpClient(Interceptor interceptor) {
        return newOkHttpBuilder(interceptor)
                .connectTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .build();
    }

    private static Request.Builder newRequestBuilder(Request original, String token) {
        Request.Builder requestBuilder = original.newBuilder()
                .addHeader("User-Agent", USER_AGENT)
                .method(original.method(), original.body());


        if (!TextUtils.isEmpty(token)) {
            requestBuilder.header("X-Auth-Token", token);
        }
        return requestBuilder;
    }

    private static OkHttpClient.Builder newOkHttpBuilder(Interceptor interceptor) {
        final HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .addNetworkInterceptor(new StethoInterceptor())
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(interceptor);

    }

}