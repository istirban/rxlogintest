package fr.pentalog.retrospectiveboard.common.http;

import retrofit2.Response;

/**
 * Created by Ionut Stirban on 03/02/2017.
 */

public class InMemoryTokenManager {
    private static final String AUTH_TOKEN_KEY = "X-Auth-Token";

    private static InMemoryTokenManager instance = new InMemoryTokenManager();
    private String token;

    public static InMemoryTokenManager getInstance() {
        return instance;
    }

    public synchronized void setTokenFromHeader(Response<?> serverResponse) {
        this.token = serverResponse.headers().get(InMemoryTokenManager.AUTH_TOKEN_KEY);

    }

    public String getToken() {
        return token;
    }
}
