package fr.pentalog.retrospectiveboard.common;

import io.reactivex.CompletableObserver;
import io.reactivex.CompletableOperator;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by Ionut Stirban on 03/02/2017.
 */

class LifecycleCompletableOperator<T> implements CompletableOperator {
    private final Observable<T> until;

    private LifecycleCompletableOperator(Observable<T> until) {
        this.until = until;
    }

    public static <T> LifecycleCompletableOperator<T> create(Observable<T> until) {
        return new LifecycleCompletableOperator<>(until);
    }

    @Override
    public CompletableObserver apply(CompletableObserver observer) throws Exception {
        return new LCompletableObserver<>(observer, until);
    }

    private static final class LCompletableObserver<T> implements CompletableObserver {
        final CompletableObserver child;
        private final Observable<T> until;

        private CompositeDisposable compositeDisposable = new CompositeDisposable();

        LCompletableObserver(CompletableObserver child, Observable<T> until) {
            this.child = child;
            this.until = until;
            subscribeToUntil();
        }

        private void subscribeToUntil() {
            compositeDisposable.add(until.subscribe(t -> {
                disposeAll();
            }));
        }

        private void disposeAll() {
            compositeDisposable.dispose();
        }

        @Override
        public void onSubscribe(Disposable d) {
            compositeDisposable.add(d);
            child.onSubscribe(d);
        }

        @Override
        public void onComplete() {
            child.onComplete();
            disposeAll();
        }

        @Override
        public void onError(Throwable e) {
            child.onError(e);
            disposeAll();
        }


    }
}

