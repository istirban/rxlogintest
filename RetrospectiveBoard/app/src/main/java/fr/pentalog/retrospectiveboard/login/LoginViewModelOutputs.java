package fr.pentalog.retrospectiveboard.login;

import fr.pentalog.retrospectiveboard.common.RxVoid;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by Ionut Stirban on 03/02/2017.
 */

public class LoginViewModelOutputs {
    public final PublishSubject<RxVoid> loginSuccess = PublishSubject.create();

    //errors
    public final PublishSubject<RxVoid> loginFailed = PublishSubject.create();
}
