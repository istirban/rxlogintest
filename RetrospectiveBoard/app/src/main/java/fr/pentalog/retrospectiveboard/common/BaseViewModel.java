package fr.pentalog.retrospectiveboard.common;

import io.reactivex.CompletableTransformer;
import io.reactivex.ObservableTransformer;

/**
 * Created by Ionut Stirban on 02/02/2017.
 */

public class BaseViewModel {
    protected ViewLifecycle viewLifecycle;
    protected AppSchedulers appSchedulers = new AndroidAppSchedulers();

    public BaseViewModel(ViewLifecycle viewLifecycle) {
        this.viewLifecycle = viewLifecycle;
    }

    public void setAppSchedulers(AppSchedulers appSchedulers) {
        this.appSchedulers = appSchedulers;
    }

    protected <T> ObservableTransformer<T, T> bindUntilDestroy() {
        return viewLifecycle.bindUntilDestroy();
    }

    protected CompletableTransformer bindCompletableUntilDestroy() {
        return viewLifecycle.bindCompletableUntilDestroy();
    }
}
