package fr.pentalog.retrospectiveboard.common;

import io.reactivex.Scheduler;

/**
 * Created by Ionut Stirban on 03/02/2017.
 */

public interface AppSchedulers {
    Scheduler io();
    Scheduler mainThread();
}
