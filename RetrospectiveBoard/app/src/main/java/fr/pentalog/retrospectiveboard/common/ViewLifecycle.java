package fr.pentalog.retrospectiveboard.common;

import io.reactivex.CompletableTransformer;
import io.reactivex.ObservableTransformer;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by Ionut Stirban on 03/02/2017.
 */

public class ViewLifecycle {
    private BehaviorSubject<Integer> viewLifecycle = BehaviorSubject.create();

    public void onResume() {
        viewLifecycle.onNext(LifecycleEvents.RESUMED);
    }

    public void onPause() {
        viewLifecycle.onNext(LifecycleEvents.PAUSED);
    }

    public void onDestroy() {
        viewLifecycle.onNext(LifecycleEvents.DESTROYED);

    }

    public <T> ObservableTransformer<T, T> bindUntilEvent(int event) {
        return upstream -> upstream.takeUntil(
                viewLifecycle.filter(integer -> integer == event));
    }

    public <T> ObservableTransformer<T, T> bindUntilDestroy() {
        return bindUntilEvent(LifecycleEvents.DESTROYED);
    }
    public CompletableTransformer bindCompletableUntilDestroy(){
        return upstream -> upstream.lift(LifecycleCompletableOperator.create(viewLifecycle.filter(event -> event == LifecycleEvents.DESTROYED)));
    }
}
