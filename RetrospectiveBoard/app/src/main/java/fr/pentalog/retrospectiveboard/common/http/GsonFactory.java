package fr.pentalog.retrospectiveboard.common.http;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializer;

import java.util.Date;

class GsonFactory {
    private GsonFactory(){
        throw new AssertionError("No instance allowed");
    }
    static Gson createGson() {
        return new GsonBuilder()
                .registerTypeHierarchyAdapter(Date.class, GsonFactory.getDeserializer())
                .registerTypeHierarchyAdapter(Date.class, GsonFactory.getSerializer())
                .create();
    }

    private static JsonDeserializer<Date> getDeserializer() {
        return (json, typeOfT, context) -> new Date(Long.valueOf((json).getAsString()));
    }

    private static JsonSerializer<Date> getSerializer() {
        return (src, typeOfSrc, context) -> new JsonPrimitive(src.getTime());
    }
}