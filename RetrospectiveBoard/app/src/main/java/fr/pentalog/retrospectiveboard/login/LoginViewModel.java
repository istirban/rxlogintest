package fr.pentalog.retrospectiveboard.login;

import fr.pentalog.retrospectiveboard.common.BaseViewModel;
import fr.pentalog.retrospectiveboard.common.RxVoid;
import fr.pentalog.retrospectiveboard.common.ViewLifecycle;
import fr.pentalog.retrospectiveboard.login.data.LoginRepository;

/**
 * Created by Ionut Stirban on 02/02/2017.
 */

public class LoginViewModel extends BaseViewModel{

    public final LoginViewModelInputs inputs = new LoginViewModelInputs();
    public final LoginViewModelOutputs outputs = new LoginViewModelOutputs();

    private LoginRepository loginRepository;

    public LoginViewModel(LoginRepository loginRepository, ViewLifecycle viewLifecycle) {
        super(viewLifecycle);
        this.loginRepository = loginRepository;
    }

    public void onLoginClicked() {
        //Log.d("test", "login clicked");
        this.loginRepository.login(inputs.name.get(), inputs.password.get())
                .compose(bindCompletableUntilDestroy())
                .doOnError(outputs.loginFailed::onError)
                .subscribe(this::loginSuccess);
    }
    private void loginSuccess(){
        outputs.loginSuccess.onNext(RxVoid.INSTANCE);
    }
    private void loginFailed(Throwable t){
        outputs.loginFailed.onNext(RxVoid.INSTANCE);
    }

}
