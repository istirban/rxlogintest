package fr.pentalog.retrospectiveboard.common;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Ionut Stirban on 03/02/2017.
 */

public class AndroidAppSchedulers implements AppSchedulers {
    @Override
    public Scheduler io() {
        return Schedulers.io();
    }

    @Override
    public Scheduler mainThread() {
        return io.reactivex.android.schedulers.AndroidSchedulers.mainThread();
    }
}
