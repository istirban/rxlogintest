package fr.pentalog.retrospectiveboard.common;

/**
 * Created by Ionut Stirban on 03/02/2017.
 */

public interface RxVoid {
    RxVoid INSTANCE = new RxVoid() {
    };
}
