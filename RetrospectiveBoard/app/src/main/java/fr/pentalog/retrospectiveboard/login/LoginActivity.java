package fr.pentalog.retrospectiveboard.login;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import fr.pentalog.retrospectiveboard.R;
import fr.pentalog.retrospectiveboard.common.BaseActivity;
import fr.pentalog.retrospectiveboard.common.RxVoid;
import fr.pentalog.retrospectiveboard.databinding.LoginActivityBinding;
import fr.pentalog.retrospectiveboard.login.data.LoginRepository;

public class LoginActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LoginActivityBinding binding = DataBindingUtil.setContentView(this, R.layout.login_activity);

        final LoginViewModel loginViewModel = new LoginViewModel(new LoginRepository(), viewLifecycle);
        binding.setViewModel(loginViewModel);

        bindToErrorsAndOutputs(loginViewModel);

    }

    private void bindToErrorsAndOutputs(LoginViewModel loginViewModel) {
        loginViewModel.outputs.loginFailed
                .compose(bindToUiUntilDestroy())
                .doOnSubscribe(__ -> Log.d("testz", "subscribed"))
                .doOnDispose(() -> Log.d("testz", "disposed"))
                .subscribe(this::showLoginFailedMessage);

        loginViewModel.outputs.loginSuccess
                .compose(bindToUiUntilDestroy())
                .subscribe(this::showLoginSuccessMessage);
    }

    public void showLoginSuccessMessage(RxVoid rxVoid) {
        Toast.makeText(this, "Successfully logged in", Toast.LENGTH_LONG).show();

    }

    public void showLoginFailedMessage(RxVoid rxVoid) {
        Toast.makeText(this, "Could not log in", Toast.LENGTH_LONG).show();
    }

    //    @Override
//    protected int getLayoutResId() {
//        return R.layout.login_activity;
//    }
}
